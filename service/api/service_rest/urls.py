from django.urls import path
from .views import list_technicians, list_appointments, show_technician, finish_appointment, cancel_appointment


urlpatterns = [
    path('technicians/', list_technicians, name='list_technicians'),
    path('appointments/', list_appointments, name='list_appointments'),
    path('technicians/<int:pk>', show_technician, name='show_technician'),
    path('appointments/<int:pk>/finish', finish_appointment, name='finish_appointment'),
    path('appointments/<int:pk>/cancel', cancel_appointment, name='cancel_appointment')
]
