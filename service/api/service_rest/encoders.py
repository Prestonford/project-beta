from .models import Technician, Appointment
from common.json import ModelEncoder

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        'id'
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "vin",
        "customer",
        "technician",
        "status"



    ]
    encoder = {
        "technician" : TechnicianEncoder(),
    }

    def get_extra_data(self, o):
        return {
            "technician" : {"id" : o.technician.id,
                                "first_name":  o.technician.first_name,
                                "last_name" : o.technician.last_name}
                                }
