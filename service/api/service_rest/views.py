from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Technician, Appointment, AutomobileVO
from .encoders import TechnicianEncoder, AppointmentEncoder
from datetime import datetime

# Create your views here.


@require_http_methods(["GET","POST"])
def list_technicians(request):
    if request.method=="GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians" : technicians},
            encoder=TechnicianEncoder
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )

@require_http_methods(["GET","DELETE"])
def show_technician(request, pk):
    if request.method=="GET":
        technician = Technician.objects.get(id=pk)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
        )
    else:
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted" : count > 0}
        )

@require_http_methods(["GET","POST"])
def list_appointments(request):
    if request.method=="GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments" : appointments},
            encoder=AppointmentEncoder
        )
    else:
        content =json.loads(request.body)
        input_date = datetime.fromisoformat(content["date"]+"T"+content["time"])
        content["date_time"] = input_date
        content["status"] = "CREATED"
        del content["time"]
        del content["date"]
        print(content)
        try:
            tech_id = content["technician"]
            technician = Technician.objects.get(id=tech_id)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Technician"},
                status=400
            )

        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )

@require_http_methods(["PUT"])
def show_appointment(request, pk):
    appointment = Appointment.objects.get(id=pk)
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False,
    )

@require_http_methods(["PUT"])
def finish_appointment(request, pk):
    appointment = Appointment.objects.get(id=pk)
    appointment.status = "FINISHED"
    appointment.save()
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False,
    )

@require_http_methods(["PUT"])
def cancel_appointment(request, pk):
    appointment = Appointment.objects.get(id=pk)
    appointment.status = "CANCELED"
    appointment.save()
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False,
    )
