# Generated by Django 4.0.3 on 2023-09-08 16:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0007_alter_status_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='name',
            field=models.CharField(choices=[(1, 'SUBMITTED'), (2, 'CANCELED'), (3, 'FINISHED')], default=1, max_length=10, unique=True),
        ),
    ]
