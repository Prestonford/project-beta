import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ListManufacturers from "./ListManufacturers";
import CreateManufacturer from "./CreateManufacturer";
import ListModels from "./ListModels";
import CreateModel from "./CreateModel";
import ListAutomobiles from "./ListAutomobiles";
import CreateAutomobile from "./CreateAutomobile";
import ListTechnicians from "./ListTechnicians";
import CreateTechnician from "./CreateTechnician";
import CreateAppointment from "./CreateAppointment";
import ListAppointments from "./ListAppointments";
import ServiceHistory from "./ServiceHistory";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/manufacturers" element={<ListManufacturers />} />
          <Route path="/manufacturer/new" element={<CreateManufacturer />} />
          <Route path="/models" element={<ListModels />} />
          <Route path="/model/new" element={<CreateModel />} />
          <Route path="/automobiles" element={<ListAutomobiles />} />
          <Route path="/automobile/new" element={<CreateAutomobile />} />
          <Route path="/technicians" element={<ListTechnicians />} />
          <Route path="/technician/new" element={<CreateTechnician />} />
          <Route path="/appointment/new" element={<CreateAppointment />} />
          <Route path="/appointments" element={<ListAppointments />} />
          <Route path="/servicehistory" element={<ServiceHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
