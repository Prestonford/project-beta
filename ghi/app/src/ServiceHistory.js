import React, { useEffect, useState } from "react";

function ServiceHistory() {
  const [autos, setAutos] = useState([]);
  let filtered = [];
  async function listAutomobiles() {
    const response = await fetch("http://localhost:8100/api/automobiles/");
    if (response.ok) {
      const vipdata = await response.json();
      setAutos(vipdata.autos);
    }
  }
  let count = 0;
  const vipautos = {};
  autos.map((auto) => {
    vipautos[auto.vin.toLowerCase()] = count;
    count += 1;
  });

  useEffect(() => {
    listAutomobiles();
  }, []);

  const [appointments, setAppointments] = useState([]);
  const [search, setSearch] = useState([]);

  async function listAppointments() {
    const response = await fetch("http://localhost:8080/api/appointments/");
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
      setSearch(data.appointments);
    }
  }
  useEffect(() => {
    listAppointments();
  }, []);
  return (
    <div>
      <h1>Service History</h1>
      <div className="input-group">
        <input
          placeholder="Search by VIN..."
          requiredtype="text"
          name="search"
          id="search"
          className="form-control"
          onChange={(e) => {
            if (e.target.value !== "") {
              const value = e.target.value.toUpperCase();
              filtered = appointments.filter((appointment) =>
                appointment.vin.includes(value)
              );
            } else {
              setSearch(appointments);
            }
          }}
        />
        <button
          className="input-group-addon"
          onClick={(e) => {
            setSearch(filtered);
          }}
        >
          Search
        </button>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Vin</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Actions:</th>
          </tr>
        </thead>
        <tbody>
          {search.map((filteredAppointment) => {
            return (
              <tr key={filteredAppointment.id} value={filteredAppointment.id}>
                <td>{filteredAppointment.vin}</td>
                <td>
                  {filteredAppointment.vin.toLowerCase() in vipautos
                    ? "Yes"
                    : "No"}
                </td>
                <td>{filteredAppointment.customer}</td>
                <td>
                  {new Date(filteredAppointment.date_time).toLocaleDateString()}
                </td>
                <td>
                  {new Date(filteredAppointment.date_time).toLocaleTimeString()}
                </td>
                <td>
                  {filteredAppointment.technician.first_name}{" "}
                  {filteredAppointment.technician.last_name}
                </td>
                <td>{filteredAppointment.reason}</td>
                <td>{filteredAppointment.status}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ServiceHistory;
