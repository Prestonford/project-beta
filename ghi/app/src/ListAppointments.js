import React, { useEffect, useState } from "react";

function ListAppointments() {
  const [autos, setAutos] = useState([]);
  async function listAutomobiles() {
    const response = await fetch("http://localhost:8100/api/automobiles/");
    if (response.ok) {
      const vipdata = await response.json();
      setAutos(vipdata.autos);
    }
  }
  let count = 0;
  const vipautos = {};
  autos.map((auto) => {
    vipautos[auto.vin.toLowerCase()] = count;
    count += 1;
  });

  useEffect(() => {
    listAutomobiles();
  }, []);

  const [appointments, setAppointments] = useState([]);
  async function listAppointments() {
    const response = await fetch("http://localhost:8080/api/appointments/");
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    }
  }
  useEffect(() => {
    listAppointments();
  }, []);

  return (
    <div>
      <h1>Service Appointments</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Vin</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Actions:</th>
          </tr>
        </thead>
        <tbody>
          {appointments
            .filter((appointment) => appointment.status === "CREATED")
            .map((filteredAppointment) => {
              return (
                <tr key={filteredAppointment.id} value={filteredAppointment.id}>
                  <td>{filteredAppointment.vin}</td>
                  <td>
                    {filteredAppointment.vin.toLowerCase() in vipautos
                      ? "Yes"
                      : "No"}
                  </td>
                  <td>{filteredAppointment.customer}</td>
                  <td>
                    {new Date(
                      filteredAppointment.date_time
                    ).toLocaleDateString()}
                  </td>
                  <td>
                    {new Date(
                      filteredAppointment.date_time
                    ).toLocaleTimeString()}
                  </td>
                  <td>
                    {filteredAppointment.technician.first_name}{" "}
                    {filteredAppointment.technician.last_name}
                  </td>
                  <td>{filteredAppointment.reason}</td>
                  <td>
                    <button
                      onClick={async () => {
                        const url = `http://localhost:8080/api/appointments/${filteredAppointment.id}/finish`;
                        const response = await fetch(url, {
                          method: "put",
                          headers: { "Content-Type": "application/json" },
                        });

                        if (response.ok) {
                          window.location.reload();
                        }
                      }}
                      className="btn finish-btn btn-success"
                      type="button"
                    >
                      Finish
                    </button>
                    <button
                      onClick={async () => {
                        const url = `http://localhost:8080/api/appointments/${filteredAppointment.id}/cancel`;
                        const response = await fetch(url, {
                          method: "put",
                          headers: { "Content-Type": "application/json" },
                        });

                        if (response.ok) {
                          window.location.reload();
                        }
                      }}
                      className="btn cancel-btn btn-danger"
                    >
                      Cancel
                    </button>
                  </td>
                </tr>
              );
            })}
        </tbody>
      </table>
    </div>
  );
}

export default ListAppointments;
