from common.json import ModelEncoder
from .models import AutomobileVO, Customer, Salesperson, Sale

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
    ]

class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
    ]


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price",
        "salesperson",
        "customer",
        "automobileVO",
    ]
    encoders = {
        "salesperson" : SalespersonEncoder(),
        "customer" : CustomerEncoder(),
        "automobileVO" : AutomobileVOEncoder(),
    }
