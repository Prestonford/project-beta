from django.urls import path

from  .views import api_salespeople, api_customers, api_sales

urlpatterns = [
    path('salespeople/', api_salespeople, name='salespeople'),
    path('customers/', api_customers, name='customers'),
    path('sales/', api_sales, name='sales'),
]
